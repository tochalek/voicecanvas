package com.voicecanvas.voicecanvasmobile;

import com.squareup.moshi.Moshi;
import com.squareup.moshi.JsonAdapter;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Interceptor;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import java.util.concurrent.TimeUnit;
import java.io.IOException;

public class AIIG_RetrofitInstance {
    private static final String BASE_URL = "https://api.openai.com/";
    private static final String API_KEY = "sk-proj-CYpnUzvcZ77Nb4aj3CrbT3BlbkFJLkyFM5qugVGWsAz4Wxxi";

    private static final OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
                    Request originalRequest = chain.request();
                    Request newRequest = originalRequest.newBuilder()
                            .addHeader("Authorization", "Bearer " + API_KEY)
                            .build();
                    return chain.proceed(newRequest);
                }
            })
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .build();

    private static final Moshi moshi = new Moshi.Builder()
            // Add custom adapters here if necessary
            .build();

    private static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build();

    public static class AIIG_Api {
        private static final ApiService AIIG_RetrofitService = retrofit.create(ApiService.class);

        public static ApiService getAIIG_RetrofitService() {
            return AIIG_RetrofitService;
        }
    }
}