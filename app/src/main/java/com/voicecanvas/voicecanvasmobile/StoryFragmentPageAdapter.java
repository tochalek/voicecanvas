package com.voicecanvas.voicecanvasmobile;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import java.util.ArrayList;

public class StoryFragmentPageAdapter extends FragmentStateAdapter {
    private ArrayList<Fragment> fragments;

    public StoryFragmentPageAdapter(FragmentActivity fragmentActivity, ArrayList<Fragment> fragmentList) {
        super(fragmentActivity);
        this.fragments = new ArrayList<>(fragmentList); // Make a copy if necessary to avoid external modifications
    }

    @Override
    public Fragment createFragment(int position) {
        if (position < fragments.size()) {
            return fragments.get(position);
        }
        throw new IllegalStateException("Position exceeds fragment list size");
    }

    @Override
    public int getItemCount() {
        return fragments.size();
    }

    // Add a fragment to the adapter
    public void addFragment(Fragment fragment) {
        fragments.add(fragment);
        notifyItemInserted(fragments.size() - 1);
    }

    // Remove a fragment from the adapter
    public void removeFragment(int position) {
        if (position < fragments.size()) {
            fragments.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, fragments.size());
        } else {
            throw new IllegalStateException("Invalid position");
        }
    }

    // Update a fragment at a specific position
    public void updateFragment(int position, Fragment fragment) {
        if (position < fragments.size()) {
            fragments.set(position, fragment);
            notifyItemChanged(position);
        } else {
            throw new IllegalStateException("Invalid position");
        }
    }
}
