package com.voicecanvas.voicecanvasmobile;

import java.util.List;

public class ResponseData {
    private List<DataItem> data;

    public ResponseData(List<DataItem> data) {
        this.data = data;
    }

    public List<DataItem> getData() {
        return data;
    }

    public void setData(List<DataItem> data) {
        this.data = data;
    }
}