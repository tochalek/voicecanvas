package com.voicecanvas.voicecanvasmobile;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Body;

public interface ApiService {

    @POST("v1/images/generations")
    Call<ResponseData> generateImage(@Body ImageGenerationRequest request);
}