package com.voicecanvas.voicecanvasmobile;

public class DataItem {
    private String url;

    public DataItem(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}