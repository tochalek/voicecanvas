package com.voicecanvas.voicecanvasmobile;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PageFragment} factory method to
 * create an instance of this fragment.
 */
public class PageFragment extends Fragment
{
    TextView pageText;
    ImageView pageImage;

    public PageFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_page, container, false);

        pageText = view.findViewById(R.id.pageText);
        pageImage = view.findViewById(R.id.pageImage);

        //TODO: use getArguments to get image and text
        //getArguments()

        //pageText.setText();

        // Inflate the layout for this fragment
        return view;
    }
}