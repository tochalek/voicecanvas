package com.voicecanvas.voicecanvasmobile;

import android.content.Context;
import android.graphics.Bitmap;
import android.icu.util.Output;
import android.media.Image;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Story implements Serializable
{
    public static final String STORY_DELIM = "__VC__";

    //Static ID Variable
    private static int CURR_ID = 1;


    //Instance Variables
    private int id;
    private Date date;
    private String title;
    private int numImages;

    //need way of storing the story
    private String storyFilename;

    public Story(String title, Date date) {
        this.id = CURR_ID++;
        this.date = date;
        this.title = title;
        this.numImages = 0;

        //TODO: may want to change this, just have it be id based?
        storyFilename = this.id + "_" + this.title;
    }

    public Story(String title, Date date, Context context, ArrayList<String> imageURLS) {
        this(title, date);

        storeImages(context, imageURLS);
    }

    public Story(String title, Date date, Context context, String storyText) {
        this(title, date);

        storeText(context, storyText);
    }

    public Story(String title, Date date, Context context, ArrayList<String> imageURLS, String storyText) {
        this(title, date);

        storeImages(context, imageURLS);
        storeText(context, storyText);
    }

    public Story(String title, Date date, Context context, List<Bitmap> bitmaps, String storyText) {
        this(title, date);

        storeImagesBitmap(context, bitmaps);
        storeText(context, storyText);
    }

    public void storeText(Context context, String storyText) {
        File file = new File(context.getFilesDir(), storyFilename + "_text");

        try {
            FileWriter fw = new FileWriter(file);
            fw.write(storyText);
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Given an arraylist of imageURLS, APPENDS onto the end of the story the images given by the urls
     * @param context
     * @param imageURLS String ArrayList of urls from DALLE
     */
    public void storeImages(Context context, ArrayList<String> imageURLS) {
        for(String imageURL : imageURLS) {
            storeImage(context, numImages++, imageURL);
        }
    }

    public void storeImagesBitmap(Context context, List<Bitmap> bitmaps) {
        for(int i=0; i<bitmaps.size(); i++) {
            storeImageBitmap(context, i, bitmaps.get(i));
        }
    }

    public void storeImageBitmap(Context context, int index, Bitmap bitmap) {
        File file = new File(context.getFilesDir(), storyFilename + "_" + index);

        try {
            OutputStream out = Files.newOutputStream(file.toPath());
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Downloads and stores file into context.getFilesDir() with filename 'storyFilename_[index]'
     * where index is the number in which the image should show up in the story
     * @param context Context of the application in which this file is being stored
     * @param index integer value of the index this image should show up
     * @param imageURL String URL of DALLE generated image
     */
    private void storeImage(Context context, int index, String imageURL) {
        File file = new File(context.getFilesDir(), storyFilename + "_" + index);

        try {
            URL url = new URL(imageURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            int responseCode = connection.getResponseCode();
            if(responseCode == HttpURLConnection.HTTP_OK) { //200
                InputStream in = connection.getInputStream();
                OutputStream out = Files.newOutputStream(file.toPath());

                byte[] buffer = new byte[4096];

                int bytesRead;
                while((bytesRead = in.read(buffer)) != -1) {
                    out.write(buffer, 0, bytesRead);
                }

                in.close();
                out.close();
            }
            else {
                Log.d("VoiceCanvas ERROR", "unable to download file");
            }

        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    public String[] getStoryByPages(Context context) {
        return getStory(context).split(STORY_DELIM);
    }

    public String getImagePath(Context context, int index) {
        File file = new File(context.getFilesDir(), storyFilename + "_" + index);
        return file.getPath();
    }

    public Date getDate() {
        return this.date;
    }

    public String getTitle() {
        return this.title;
    }

    /* Debug Stuff */
    public String getImageData(Context context, int index) {
        File file = new File(context.getFilesDir(), storyFilename + "_" + index);

        try {
            FileReader fr = new FileReader(file);
            char[] buffer = new char[2048];
            if(fr.read(buffer) == -1) {
                return "NONE";
            }
            return new String(buffer);
        } catch(IOException e) {
            e.printStackTrace();
            return "ERROR";
        }
    }

    public String getStory(Context context) {
        File file = new File(context.getFilesDir(), storyFilename + "_text");
        StringBuilder sb = new StringBuilder();

        try {
            FileReader reader = new FileReader(file);
            char[] buffer = new char[4096];
            int charsRead;
            while((charsRead = reader.read(buffer)) != -1) {
                sb.append(buffer, 0, charsRead);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return "ERROR";
        }

        return sb.toString();
    }
}
