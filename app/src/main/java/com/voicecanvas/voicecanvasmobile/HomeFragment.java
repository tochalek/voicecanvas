package com.voicecanvas.voicecanvasmobile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import android.graphics.Bitmap;
import androidx.annotation.Nullable;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.ai.client.generativeai.GenerativeModel;
import com.google.ai.client.generativeai.java.GenerativeModelFutures;
import com.google.ai.client.generativeai.type.Content;
import com.google.ai.client.generativeai.type.GenerateContentResponse;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import retrofit2.Call;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {
    private TextView storyTextView;
    private EditText editText;
    private Button speechBtn;
    private SpeechRecognizer speechRecognizer;
    private boolean isListening = false;
    private GenerativeModelFutures model;

    //Store Stuff
    private String prompt;
    private String storyText;

    private String title;


    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_home, container, false);

        editText = (EditText) view.findViewById(R.id.editText);
        speechBtn = (Button) view.findViewById(R.id.btnSpeak);
        storyTextView = (TextView) view.findViewById(R.id.storyTextView);

        GenerativeModel gm = new GenerativeModel("gemini-pro", "AIzaSyCYRwJXnC14UB_JEDF2aiZ6snbUtdKl3dI");
        model = GenerativeModelFutures.from(gm);

        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(getActivity());
        Intent speechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        speechRecognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onResults(Bundle results) {
                ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                if (matches != null) {
                    String recognizedText = matches.get(0);
                    editText.setText(recognizedText);
                    prompt = recognizedText;
                    generateStory(recognizedText);
                    generateTitle(recognizedText);
                }
                isListening = false;
                updateButtonText();
            }

            @Override
            public void onReadyForSpeech(Bundle params) {
                isListening = true;
                updateButtonText();
            }

            @Override
            public void onBeginningOfSpeech() {
                editText.setText("");
                editText.setHint("Listening...");
                isListening = true;
                updateButtonText();
            }

            @Override
            public void onRmsChanged(float rmsdB) {
            }

            @Override
            public void onBufferReceived(byte[] buffer) {
            }

            @Override
            public void onEndOfSpeech() {
            }

            @Override
            public void onError(int error) {
                isListening = false;
                updateButtonText();
            }

            @Override
            public void onPartialResults(Bundle partialResults) {
            }

            @Override
            public void onEvent(int eventType, Bundle params) {
            }
        });


        speechBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isListening) {
                    speechRecognizer.stopListening();
                    isListening = false;
                } else {
                    speechRecognizer.startListening(speechRecognizerIntent);
                    isListening = true;
                }
                updateButtonText();
            }
        });


        return view;
    }

    private void updateButtonText() {
        speechBtn.setText(isListening ? "Stop" : "Speak");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (speechRecognizer != null) {
            speechRecognizer.destroy();
        }
    }
    private void generateTitle(String promptText){
        SharedPreferences prefs = getActivity().getSharedPreferences("AppSettings", Context.MODE_PRIVATE);
        String ageRange = prefs.getString("ageRange", "3-5 years");
        String theme = prefs.getString("theme", "Space Adventure");
        String additionalPrompt = " For age range: " + ageRange + ", with a " + theme + " theme ";
        Content content = new Content.Builder()
                .addText("create me a title for a children book based on the following" + promptText + additionalPrompt + "Output just the title")
                .build();
        Executor executor = Executors.newSingleThreadExecutor();

        ListenableFuture<GenerateContentResponse> response = model.generateContent(content);
        Futures.addCallback(response, new FutureCallback<GenerateContentResponse>() {
            @Override
            public void onSuccess(GenerateContentResponse result) {
                String resultText = result.getText();
                System.out.println("RESULT TITLE" + resultText);
                title = resultText;

                Log.d("DONE WITH TEST", "DONE WITH TEST");

            }

            @Override
            public void onFailure(Throwable t) {
                getActivity().runOnUiThread(() -> {
                    storyTextView.setText("Failed to generate title.");
                    t.printStackTrace();
                });
            }
        }, executor);

    }
    private void generateStory(String promptText) {
        SharedPreferences prefs = getActivity().getSharedPreferences("AppSettings", Context.MODE_PRIVATE);
        String ageRange = prefs.getString("ageRange", "3-5 years");
        String theme = prefs.getString("theme", "Space Adventure");
        String imageStyle = prefs.getString("imageStyle", "Cartoonish");
        String additionalPrompt = " For age range: " + ageRange + ", with a " + theme + " theme, and " + imageStyle + " imagery. ";

        Content content = new Content.Builder()
                .addText("create me a simple and picture book story about" + promptText + additionalPrompt + "Output the text in a way such that it would be easy to create a picture book (should still have text but not so text heavy) so divide each paragraph with '" + Story.STORY_DELIM + "'. also try to ensure their is some moral to the story. like there is some difficulty and they overcame it")
                .build();

        Executor executor = Executors.newSingleThreadExecutor();

        ListenableFuture<GenerateContentResponse> response = model.generateContent(content);
        Futures.addCallback(response, new FutureCallback<GenerateContentResponse>() {
            @Override
            public void onSuccess(GenerateContentResponse result) {
                String resultText = result.getText();
                storyText = resultText;
                generateImages(resultText);
                getActivity().runOnUiThread(() -> storyTextView.setText(resultText)); // Update UI on the main thread


                // Testing that Story object works
                String bucks = "https://upload.wikimedia.org/wikipedia/en/thumb/4/4a/Milwaukee_Bucks_logo.svg/1200px-Milwaukee_Bucks_logo.svg.png";
                String bucks2 = "https://cdn.nba.com/teams/uploads/sites/1610612749/2024/03/MicrosoftTeams-image.jpg";
                String storyText = "This is the text of the story. " + Story.STORY_DELIM + "This is the second page!";

                ArrayList<String> images = new ArrayList<String>();
                images.add(bucks);
                images.add(bucks2);
                MainActivity.stories.add(new Story("Test Story", new Date(), getContext(), images, storyText));

                Log.d("DONE WITH TEST", "DONE WITH TEST");
            }

            @Override
            public void onFailure(Throwable t) {
                getActivity().runOnUiThread(() -> {
                    storyTextView.setText("Failed to generate story.");
                    t.printStackTrace();
                });
            }
        }, executor);
    }

    private void generateImages(String story) {
        // Assuming story is divided by some delimiter or you extract key phrases manually
        String tempStory = "Create 4 images equal size that represent this story: " + story + ": There should be no words in the image and prioritize the 4 images.";
        String[] prompts = tempStory.split(";"); // Adjust the split method to suit your story's format

        for (String prompt : prompts) {
            ImageGenerationRequest request = new ImageGenerationRequest("dall-e-3", prompt.trim(), 1, "1024x1024");

            Call<ResponseData> call = AIIG_RetrofitInstance.AIIG_Api.getAIIG_RetrofitService().generateImage(request);
            call.enqueue(new retrofit2.Callback<ResponseData>() {
                @Override
                public void onResponse(Call<ResponseData> call, retrofit2.Response<ResponseData> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        List<DataItem> imageData = response.body().getData();
                        for (DataItem item : imageData) {
                            // Update your UI or process data
                            Log.d("Image URL", item.getUrl());
                            createImages(item.getUrl());
                        }
                    } else {
                        // Check response error body or status code to determine the error nature
                        if (response.errorBody() != null) {
                            try {
                                String errorStr = response.errorBody().string();
                                Log.e("API Call", "Response not successful: " + errorStr);
                                // Optionally parse the error string to JSON or handle it depending on the API response structure
                            } catch (IOException e) {
                                Log.e("API Call", "Error while reading error body", e);
                            }
                        } else {
                            Log.e("API Call", "Response not successful. Code: " + response.code());
                        }
                    }
                }
                @Override
                public void onFailure(Call<ResponseData> call, Throwable t) {
                    Log.e("API Call", "Failed to generate image", t);
                }
            });
        }
    }

    private void createImages(String imageUrl) {
        // Assuming 'imageUrl' is the URL to the image that needs to be split
        Glide.with(getContext())
                .asBitmap()
                .load(imageUrl) // Load the full image as a Bitmap
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        List<Bitmap> splitImages = splitBitmap(resource); // Split the image into four parts
                        displayImages(splitImages); // Display the split images
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                        Log.e("Glide", "Failed to load image.");
                    }
                });
    }

    private List<Bitmap> splitBitmap(Bitmap bitmap) {
        List<Bitmap> bitmaps = new ArrayList<>();
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        // Assuming the images are laid out in a grid (2x2)
        int subImageWidth = width / 2;
        int subImageHeight = height / 2;

        // Create sub-images
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                int x = j * subImageWidth;
                int y = i * subImageHeight;
                bitmaps.add(Bitmap.createBitmap(bitmap, x, y, subImageWidth, subImageHeight));
            }
        }
        return bitmaps;
    }

    private void displayImages(List<Bitmap> images) {
        LinearLayout imagesLayout = getView().findViewById(R.id.imagesLayout); // Make sure you have this LinearLayout in your layout XML
        imagesLayout.removeAllViews(); // Clear existing images
        System.out.println(images.size());
        ImageView imageView = new ImageView(getContext());
        imageView.setImageBitmap(images.get(3));
        imagesLayout.addView(imageView);

        //launch activity stuff
        Story newStory = new Story(prompt, new Date(), getContext(), images, storyText);
        MainActivity.stories.add(newStory);

        Intent intent = new Intent(getContext(), ViewStoryActivity.class);
        intent.putExtra("story", newStory);

        getContext().startActivity(intent);
    }
}