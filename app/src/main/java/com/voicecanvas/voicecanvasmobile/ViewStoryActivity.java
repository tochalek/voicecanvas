package com.voicecanvas.voicecanvasmobile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import java.util.ArrayList;

public class ViewStoryActivity extends AppCompatActivity {

    private ViewPager2 viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_story);

        TextView textView = findViewById(R.id.storyTitle);
        viewPager = findViewById(R.id.viewStoryViewPager);

        Intent intent = getIntent();
        Story story = (Story) intent.getSerializableExtra("story");

        //TODO: add stuff to view pager
        String[] textPages = story.getStoryByPages(this);
        ArrayList<Fragment> fragments = new ArrayList<Fragment>();


        viewPager.setAdapter(new StoryFragmentPageAdapter(this, fragments));
        //TODO: some sort of checking for null somehow?
        textView.setText(story.getTitle());

    }
}