package com.voicecanvas.voicecanvasmobile;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class HistoryViewAdapter extends RecyclerView.Adapter<HistoryViewAdapter.HistoryViewHolder>
{
    private final Context context;
    private ArrayList<Story> storyList;

    //need a way for storing images somehow
    //private ArrayList<String> imageList;

    public HistoryViewAdapter(Context context, ArrayList<Story> storyList) {
        this.context = context;
        this.storyList = storyList;
    }

    @NonNull
    @Override
    public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_list_item, parent, false);
        return new HistoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryViewHolder holder, int position) {
        Story story = storyList.get(position);
        holder.textView.setText(story.getTitle());
        holder.imageView.setImageResource(R.drawable.ic_launcher_foreground);
    }

    @Override
    public int getItemCount() {
        return storyList.size();
    }

    public class HistoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public TextView textView;
        public ImageView imageView;

        public HistoryViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.listItemTextView);
            imageView = itemView.findViewById(R.id.listItemImageView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int indexClicked = getLayoutPosition();

            Intent intent = new Intent(v.getContext(), ViewStoryActivity.class);
            Story story = storyList.get(indexClicked);
            intent.putExtra("story", story);

            v.getContext().startActivity(intent);
        }
    }
}
