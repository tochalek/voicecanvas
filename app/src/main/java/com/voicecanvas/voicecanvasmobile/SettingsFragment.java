package com.voicecanvas.voicecanvasmobile;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import androidx.fragment.app.Fragment;

public class SettingsFragment extends Fragment {
    private Spinner spinnerAgeRange, spinnerTheme, spinnerImageStyle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        spinnerAgeRange = view.findViewById(R.id.spinnerAgeRange);
        spinnerTheme = view.findViewById(R.id.spinnerTheme);
        spinnerImageStyle = view.findViewById(R.id.spinnerImageStyle);

        initializeSpinner(spinnerAgeRange, R.array.age_ranges);
        initializeSpinner(spinnerTheme, R.array.themes);
        initializeSpinner(spinnerImageStyle, R.array.image_styles);

        spinnerAgeRange.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                savePreference("ageRange", parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spinnerTheme.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                savePreference("theme", parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spinnerImageStyle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                savePreference("imageStyle", parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        loadSettings();

        return view;
    }

    private void loadSettings() {
        SharedPreferences prefs = getActivity().getSharedPreferences("AppSettings", Context.MODE_PRIVATE);
        setSpinnerToValue(spinnerAgeRange, prefs.getString("ageRange", "3-5 years"));
        setSpinnerToValue(spinnerTheme, prefs.getString("theme", "Space Adventure"));
        setSpinnerToValue(spinnerImageStyle, prefs.getString("imageStyle", "Cartoonish"));
    }

    private void setSpinnerToValue(Spinner spinner, String value) {
        ArrayAdapter<CharSequence> adapter = (ArrayAdapter<CharSequence>) spinner.getAdapter();
        int position = adapter.getPosition(value);
        spinner.setSelection(position, true);
    }


    private void initializeSpinner(Spinner spinner, int arrayResourceId) {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                arrayResourceId, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    private void savePreference(String key, String value) {
        SharedPreferences prefs = getActivity().getSharedPreferences("AppSettings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.apply();
    }
}

